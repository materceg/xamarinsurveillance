using Android.Graphics;
using System;

namespace XamarinSurveillance
{
    public static class BitmapHelpers
    {
        public static Bitmap LoadAndResizeBitmap(string fileName, int width, int height)
        {
            // First we get the the dimensions of the file on disk
            BitmapFactory.Options options = new BitmapFactory.Options { InJustDecodeBounds = true };
            BitmapFactory.DecodeFile(fileName, options);

            // Next we calculate the ratio that we need to resize the image by
            // in order to fit the requested dimensions.
            int outHeight = options.OutHeight;
            int outWidth = options.OutWidth;
            int inSampleSize = 1;

            if (outHeight > height || outWidth > width)
            {
                inSampleSize = outWidth > outHeight
                                   ? outHeight / height
                                   : outWidth / width;
            }

            // Now we will load the image and have BitmapFactory resize it for us.
            options.InSampleSize = inSampleSize;
            options.InJustDecodeBounds = false;
            Bitmap resizedBitmap = BitmapFactory.DecodeFile(fileName, options);

            return resizedBitmap;
        }

        public static int CompareBitmaps(Bitmap b1, Bitmap b2)
        {
            int SAD = -1;
            
            if (b1 != null && b2 != null && b1.Width == b2.Width && b1.Height == b2.Height)
            {
                SAD = 0;
                Bitmap b1Resize = ResizeBitmap(b1, 8, 6);
                Bitmap b2Resize = ResizeBitmap(b2, 8, 6);
                int[] pixels1 = new int[b1Resize.Width * b1Resize.Height];
                int[] pixels2 = new int[b2Resize.Width * b2Resize.Height];
                b1Resize.GetPixels(pixels1, 0, b1Resize.Width, 0, 0, b1Resize.Width, b1Resize.Height);
                b2Resize.GetPixels(pixels2, 0, b2Resize.Width, 0, 0, b2Resize.Width, b2Resize.Height);

                byte[] pixels1R = new byte[pixels1.Length];
                byte[] pixels1G = new byte[pixels1.Length];
                byte[] pixels1B = new byte[pixels1.Length];

                byte[] pixels2R = new byte[pixels2.Length];
                byte[] pixels2G = new byte[pixels2.Length];
                byte[] pixels2B = new byte[pixels2.Length];

                for (int i = 0; i < pixels1.Length; i++)
                {
                    pixels1R[i] = (byte)((pixels1[i] >> 16) & 0xff);
                    pixels1G[i] = (byte)((pixels1[i] >> 8) & 0xff);
                    pixels1B[i] = (byte)(pixels1[i] & 0xff);
                    pixels2R[i] = (byte)((pixels2[i] >> 16) & 0xff);
                    pixels2G[i] = (byte)((pixels2[i] >> 8) & 0xff);
                    pixels2B[i] = (byte)(pixels2[i] & 0xff);
                }

                for (int i = 0; i < pixels1.Length; i++)
                {                  
                    SAD += Math.Abs(pixels2R[i] - pixels1R[i]);
                    SAD += Math.Abs(pixels2G[i] - pixels1G[i]);
                    SAD += Math.Abs(pixels2B[i] - pixels1B[i]);
                }
                b1Resize.Recycle();
                b2Resize.Recycle();
                GC.Collect();
            }
            return SAD;
        }


        public static Bitmap ResizeBitmap(Bitmap bm, int newWidth, int newHeight)
        {
            int width = bm.Width;
            int height = bm.Height;
            float scaleWidth = ((float)newWidth) / width;
            float scaleHeight = ((float)newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.PostScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.CreateBitmap(
                bm, 0, 0, width, height, matrix, false);
            return resizedBitmap;
        }



    }
}