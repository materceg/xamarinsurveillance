using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Bitmap = Android.Graphics.Bitmap;

namespace XamarinSurveillance.Resources.helperClasses
{
    interface ICameraAPI
    {
        Bitmap GetPicture();
        void TakePicture();
        void Open();
        void Close();
    }
}