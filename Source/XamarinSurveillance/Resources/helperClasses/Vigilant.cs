using Android.OS;
using Android.Widget;
using Java.Lang;
using Process = Android.OS.Process;
using Bitmap = Android.Graphics.Bitmap;
using Android.App;
using System;
using Android.Media;

namespace XamarinSurveillance.Resources.helperClasses
{
    class Vigilant: Thread
    {
        ICameraAPI camera;
        SeekBar intervalBar;
        Handler parentUIHandler;
        Bitmap bitmapOld;
        Bitmap bitmapNew;

        public Vigilant(ICameraAPI camera, SeekBar bar, Handler UIHandler)
        {       
            this.camera = camera;
            intervalBar = bar;
            parentUIHandler = UIHandler;
            bitmapOld = bitmapNew = null;
        }

        override
        public void Run()
        {
            Process.SetThreadPriority(ThreadPriority.Background);

            /*
            FaceDetector.Face[] faces = new FaceDetector.Face[5];  
            FaceDetector detector = new FaceDetector(640,480, 5);
            detector.FindFaces(bitmapNew, faces);
            */
            camera.Open();

            while (true)
            {                               
                long time =  SystemClock.UptimeMillis();
                camera.TakePicture();

                // Hugely important, this will release the previously used Bitmap from memory, and garbage collect immediately
                // Without this, OutOfMemoryError is guaranteed very fast
                if (bitmapOld != null)
                {
                    bitmapOld.Recycle();
                    GC.Collect();
                }
                bitmapOld = bitmapNew;

                do
                {
                    bitmapNew = camera.GetPicture();
                } while (bitmapNew == null);
              
                //send new Bitmap to UI thread image view via handler               
                Message msg = Message.Obtain();
                msg.What = 0;
                msg.Obj = bitmapNew;
                parentUIHandler.SendMessage(msg);

                if (bitmapOld != null)
                {
                    int SAD = BitmapHelpers.CompareBitmaps(bitmapOld, bitmapNew);
                    // (8*6 pixels, 3 bytes each, 0-255 range of values in bytes), 25 is 10% of value, so SAD of 25*144 = 3600 is around 10% difference 
                    if (SAD > 800)                                      
                        parentUIHandler.SendEmptyMessage(1);                                         
                }

                if (time + GetInterval() > SystemClock.UptimeMillis())
                    SystemClock.Sleep((time + GetInterval()) - SystemClock.UptimeMillis());

                if (IsInterrupted)
                {
                    camera.Close();
                    if (bitmapNew != null)
                        bitmapNew.Recycle();
                    if (bitmapOld != null)
                        bitmapOld.Recycle();
                    return;
                }
            }
        }



        private int GetInterval()
        {
            int interval = 0;
            if (intervalBar.Progress != 0)
                interval = (int)((double)intervalBar.Progress / 10.0 * (1000));
            return interval;
        }


    }
}