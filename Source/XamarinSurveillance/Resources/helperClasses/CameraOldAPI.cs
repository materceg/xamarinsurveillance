using Android.Content;
using Android.Hardware;
using Android.Views;
using static Android.Hardware.Camera;
using Bitmap = Android.Graphics.Bitmap;
using BitmapFactory = Android.Graphics.BitmapFactory;

namespace XamarinSurveillance.Resources.helperClasses
{
    class CameraOldAPI: Java.Lang.Object, ICameraAPI, IPictureCallback 
    {      
        Camera camera;
        Context context;
        SurfaceView dummyView;
        Bitmap bitmap;
        bool pictureReady = false;       

        public CameraOldAPI(Context context)
        {
            this.context = context;
            dummyView = new SurfaceView(context);
            pictureReady = false;
        }

        public void Open()
        {            
            camera = Camera.Open(0);

            // Set low quality images (faster image capture speed) and no rotation
            Parameters parameters = camera.GetParameters();
            parameters.JpegQuality = 10;
            parameters.SetPictureSize(320,240);
            parameters.SetRotation(0);
            // parameters.Set("mode", "m");
            // parameters.Set("aperture", "28"); //can be 28 32 35 40 45 50 56 63 71 80 on default zoom
            // parameters.Set("shutter-speed", 9); // depends on camera, eg. 1 means longest
            // parameters.Set("iso", 150);
            // This increases speed
            parameters.ExposureCompensation = parameters.MinExposureCompensation;
            camera.SetParameters(parameters);          
            camera.SetPreviewDisplay(dummyView.Holder);
        }

        public void Close()
        {
            camera.Release();
        }

        public Bitmap GetPicture()
        {
            if (pictureReady)
                return bitmap;
            else
                return null;
        }

        public void TakePicture()
        {
            pictureReady = false;      
            camera.StartPreview();
            camera.TakePicture(null, null, this);           
        }

        // Callback method for camera
        public void OnPictureTaken(byte[] data, Camera camera)
        {
            camera.StopPreview();  // this seems to be automatic          
            bitmap = BitmapFactory.DecodeByteArray(data, 0, data.Length); // returns ARGB_8888 format
            pictureReady = true;
        }








    }
}