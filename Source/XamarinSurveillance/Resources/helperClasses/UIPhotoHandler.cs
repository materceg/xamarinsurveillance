using Android.OS;
using Android.Widget;
using Android.Graphics;
using Android.App;
using Android.Views;
using Java.Util;
using Java.Lang;
using Java.IO;
using Refractored.Xam.Vibrate;

namespace XamarinSurveillance.Resources.helperClasses
{
    class UIPhotoHandler: Handler
    {
        ImageView pictureFrame;
       
        public ImageView PictureFrame
        {
            get
            {
                return pictureFrame;
            }

            set
            {
                pictureFrame = value;
            }
        }
       
        public UIPhotoHandler(Looper looper, ImageView frame) : base(looper) {
            PictureFrame = frame;          
        }

        override
        public void HandleMessage(Message message)
        {
            if (message.What == 0)
                PictureFrame.SetImageBitmap((Bitmap)message.Obj);
            else if (message.What == 1)
            {                
                Toast newToast = Toast.MakeText(Application.Context, "Change detected", ToastLength.Long);
                newToast.SetGravity(GravityFlags.Bottom, 0, -20);
                newToast.Show();
               // Vibrator vibrator = (Vibrator)Activity.GetSystemService(Application.VibratorService);
               // vibrator.Vibrate(100);
                var v = CrossVibrate.Current;
                v.Vibration();
            }
                message.Recycle();
        }

        

        /*
        private void takeScreenshot()
        {
            Date now = new Date();
            Android.Text.Format.DateFormat.Format("yyyy-MM-dd_hh:mm:ss", now);

            try
            {
                // image naming and path  to include sd card  appending name you choose for file
                string mPath = Environment.ExternalStorageDirectory.ToString() + "/" + now + ".jpg";

                // create bitmap screen capture
                View v1 =  Window. GetDecorView().getRootView();
                v1.DrawingCacheEnabled = true;
                Bitmap bitmap = Bitmap.CreateBitmap(v1.DrawingCache);
                v1.DrawingCacheEnabled = false;

                File imageFile = new File(mPath);

                FileOutputStream outputStream = new FileOutputStream(imageFile);
                int quality = 100;
                bitmap.Compress(Bitmap.CompressFormat.Jpeg, quality, outputStream);
                outputStream.Flush();
                outputStream.Close();              
            }
            catch (Throwable e)
            {
                // Several error may come out with file handling or OOM
                e.printStackTrace();
            }
        }
        */
    }
}