﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using XamarinSurveillance.Resources.helperClasses;
using Java.Lang;
using Android.Graphics;

namespace XamarinSurveillance
{

    [Activity(Label = "XamarinSurveillance", MainLauncher = true, Icon = "@drawable/xamarinsurveillance1")]
    public class MainActivity : Activity
    {              
        ImageView pictureFrame;
        ICameraAPI camera;
        Handler photoHandler;
        Thread backgroundThread;      
        
        #region Getters/Setters
        public ImageView PictureFrame
        {
            get
            {
                return pictureFrame;
            }

            set
            {
                pictureFrame = value;
            }
        }

        internal ICameraAPI Camera
        {
            get
            {
                return camera;
            }

            set
            {
                camera = value;
            }
        }

        public Handler PhotoHandler
        {
            get
            {
                return photoHandler;
            }

            set
            {
                photoHandler = value;
            }
        }
        #endregion

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            RequestedOrientation = ScreenOrientation.Portrait; // Force portrait            
            SetContentView(Resource.Layout.Main);
           
            PictureFrame = FindViewById<ImageView>(Resource.Id.capturedImageView);
            PictureFrame.SetBackgroundColor(Color.Rgb(255, 150, 50));
            PictureFrame.SetImageBitmap(null);
            FindViewById<Button>(Resource.Id.EngageButton).Click += StartSurveillance;
            Camera = new CameraOldAPI(this);          
            PhotoHandler = new UIPhotoHandler(Looper.MainLooper, PictureFrame);
        }

        protected override void OnPause()
        {
            base.OnPause();
        }

        protected override void OnResume()
        {
            base.OnResume();
        }           

        protected void StartSurveillance(object sender, EventArgs eventArgs)
        {
            backgroundThread = new Vigilant(Camera, FindViewById<SeekBar>(Resource.Id.IntervalBar), PhotoHandler);           
            backgroundThread.Start();            
            ((Button)sender).Click -= StartSurveillance;
            ((Button)sender).Click += StopSurveillance;
            ((Button)sender).Text = "Disarm";
        } 

        protected void StopSurveillance(object sender, EventArgs eventArgs)
        {           
            backgroundThread.Interrupt();
            backgroundThread = null;
            ((Button)sender).Click -= StopSurveillance;
            ((Button)sender).Click += StartSurveillance;
            ((Button)sender).Text = "Arm";
        }
    }
}

